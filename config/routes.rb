Rails.application.routes.draw do  
  root 'page#index'

  devise_for :users, controllers: {
    registrations: "users/registrations"
  }

  get "notifications" => "page#notifications"

  resources :posts, only: [:create, :show] do
    post "like", on: :member
    
    resources :comments, only: :create do
      member do
        post "comment"
        post "like"
      end
    end
  end
end
