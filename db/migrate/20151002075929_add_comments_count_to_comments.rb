class AddCommentsCountToComments < ActiveRecord::Migration
  def self.up
    add_column :comments, :comments_count, :integer, default: 0

    Comment.all.each do |c|
      c.update(comments_count: c.comments.count)
    end
  end

  def self.down
    remove_column :comments, :comments_count
  end
end
