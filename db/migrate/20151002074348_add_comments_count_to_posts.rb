class AddCommentsCountToPosts < ActiveRecord::Migration
  def self.up
    add_column :posts, :comments_count, :integer, default: 0

    Post.all.each do |p|
      p.update(comments_count: p.comments.count)
    end
  end

  def self.down
    remove_column :posts, :comments_count
  end
end
