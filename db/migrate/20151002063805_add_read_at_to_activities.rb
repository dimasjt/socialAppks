class AddReadAtToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :read_at, :datetime
  end
end
