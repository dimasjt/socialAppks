module ApplicationHelper
  def count_notifications(notifications)
    notifications.where(read_at: nil).count
  end

  def readed(read_at)
    'active' unless read_at
  end
end
