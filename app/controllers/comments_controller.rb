class CommentsController < ApplicationController
  before_action :authenticate_user!, only: [:create, :like, :comment]
  before_action :set_comment, only: [:show, :edit, :update, :destroy, :like]

  # GET /comments
  def index
    @comments = Comment.all
  end

  # GET /comments/1
  def show
  end

  # GET /comments/new
  def new
    @comment = Comment.new
  end

  # GET /comments/1/edit
  def edit
  end

  # POST /comments
  def create
    post = Post.find params[:post_id]

    @comment = post.comments.create(comment_params)
    @comment.user_id = current_user.id

    if @comment.save
      post.create_activity key: 'post.comment', owner: current_user, recipient: post.user unless post.user == current_user

      flash[:success] = 'Comment was successfully created.'
      redirect_to post_url(post)
    else
      render :new
    end
  end

  # PATCH/PUT /comments/1
  def update
    if @comment.update(comment_params)
      redirect_to @comment, notice: 'Comment was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /comments/1
  def destroy
    @comment.destroy
    redirect_to comments_url, notice: 'Comment was successfully destroyed.'
  end

  def comment
    post = Post.find params[:post_id]
    comment = Comment.find params[:id]

    @comment = comment.comments.create(comment_params)
    @comment.user_id = current_user.id

    if @comment.save
      comment.create_activity key: 'comment.comment', owner: current_user, recipient: comment.user unless comment.user == current_user

      flash[:success] = 'Comment was successfully created.'
      redirect_to post_url(post)
    else
      flash[:danger] = 'Comment was failed to create.'
      redirect_to post_url(post)
    end
  end

  def like
    @post = Post.find params[:post_id]
    @comment.like_by current_user
    
    @comment.create_activity key: 'comment.like', owner: current_user, recipient: @comment.user unless @comment.user == current_user

    flash[:success] = 'Successfully like this post'
    redirect_to post_url(@post)
  end

  private
  
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def comment_params
      params.require(:comment).permit(:comment)
    end
end
