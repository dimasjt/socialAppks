class PageController < ApplicationController
  after_action :set_read_at, only: :notifications

  def index
    @post = Post.new
    @posts = Post.order(created_at: :desc).page(params[:page])
  end

  def notifications
  end

  private
  
    def set_read_at
      notifs = @notifications.where(read_at: nil)
      @notifications.where(read_at: nil).update_all(read_at: Time.now) unless notifs.empty?
    end
end
