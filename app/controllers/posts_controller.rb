class PostsController < ApplicationController
  before_action :authenticate_user!, only: [:create, :like]
  before_action :set_post, only: [:show, :edit, :update, :destroy, :like]

  # GET /posts
  def index
    @posts = Post.all
  end

  # GET /posts/1
  def show
    @comment = Comment.new
    @comments = @post.comments.order(created_at: :desc).page(params[:page])
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  def create
    @post = current_user.posts.new(post_params)

    if @post.save
      flash[:success] = 'Post was successfully created.'
      redirect_to root_url
    else
      render :new
    end
  end

  # PATCH/PUT /posts/1
  def update
    if @post.update(post_params)
      redirect_to @post, notice: 'Post was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /posts/1
  def destroy
    @post.destroy
    redirect_to posts_url, notice: 'Post was successfully destroyed.'
  end

  def like
    @post.like_by current_user

    @post.create_activity key: 'post.like', owner: current_user, recipient: @post.user unless @post.user == current_user

    flash[:success] = 'Successfully like this post'
    redirect_to post_url(@post)
  end

  private
  
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def post_params
      params.require(:post).permit(:user_id, :content)
    end
end
