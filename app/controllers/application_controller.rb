class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :set_notifications

  protected

  def set_notifications
    @notifications = PublicActivity::Activity.where(recipient_id: current_user.id).order(created_at: :desc) if user_signed_in?
  end
end
