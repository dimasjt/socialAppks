class Post < ActiveRecord::Base
  include PublicActivity::Model
  
  acts_as_votable
  acts_as_commentable
  
  belongs_to :user
end
