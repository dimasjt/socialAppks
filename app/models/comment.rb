class Comment < ActiveRecord::Base
  include PublicActivity::Model
  include ActsAsCommentable::Comment
  
  acts_as_votable
  acts_as_commentable

  # default_scope -> { order('created_at ASC') }

  belongs_to :commentable, :polymorphic => true, :counter_cache => :comments_count
  belongs_to :user
end
